# Frontend

Frontend of the BMS

## Demo

https://bms.jobsintown.org

## Requirements

* Running strpi Backend located at: https://gitlab.com/pwrk/bewerbermanagement/bms-backend
* [node](https://nodejs.org/) and [yarn](https://yarnpkg.com/getting-started/install)

## Install

install dependencies and start a development environment by:

```bash
yarn && yarn dev
```

Browser should open at http://localhost:9000

## Configuration

`.env` contains settings


