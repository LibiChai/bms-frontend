/*
 Shared tranlations key should be at root level, and other translations
 should be categorized according to the component/page where they are used.
*/

export default {
  forgot_password:
    'We will send an email with a link to reset the password for the email address you entered.',
  dark_mode: 'Switch dark display on and off',
  visibility: 'Hide salary in job advertisement',
  reference:
    'You can assign a reference number to your advertisement. An applicant can refer to this.',
  apply_post:
    'Here you can specify whether you would prefer to receive applications in the traditional form by post. The application button in the job advertisement then adjusts accordingly.',
};
