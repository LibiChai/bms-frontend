import btn from './btn';
import label from './label';
import rules from './rules';
import help from './help';
import menu from './menu';

export default {
  localeName: 'English',
  french: 'French',
  german: 'German',
  english: 'English',
  jobs: 'Jobs',
  logo: 'Logo',
  create_job: 'Create new job',
  edit_job: 'Edit job',
  header: 'Headerimage',
  jobboard: 'Jobboard',
  job_title: 'Job title',
  ad_management: 'Ad management',
  team_management: 'Team management',
  invite_new_user: 'Invite team members',
  public_jobs: 'Current job offers',
  settings: 'Settings',
  templates: 'Templates',
  date: 'Date',
  company: 'Company',
  recruiter: 'Recruiter',
  title: 'Please register',
  action: 'Action',
  unpublished: 'unpublished',
  'ad-management-description':
    'With the advertisement management you can save, edit and delete job advertisements. The advertisement management is also required to integrate job advertisements into your homepage.',
  preis1: 'Registration is free of charge',
  please_register:
    'You are currently not logged in. To use the ad management, you must register. Registration is free of charge.',
  confirm_del: 'Do you really want to delete the job advertisement?',
  alert: 'Alert',
  wizard_help_title: 'Create job ad',
  wizard_help_text:
    'The Jobwizard supports you in creating an advertisement. The use of the Job Wizard and the creation of the advertisement is free of charge. You can activate and deactivate further fields via the settings.',
  wizard_help_anonymous:
    'You are currently not logged in. You can use all functions as an anonymous user. However, at the end you can only download the advertisement as HTML.',
  select_or_create: 'Choose a company or create a new one.',
  welcome: 'Willkommen',
  user_profile: 'User profile',
  company_profile: 'Company profile',
  darkmode: {
    label: 'Presentation',
    light: 'Light Design',
    dark: 'Dark Design',
    auto: 'auto',
  },
  nav: {
    applications: 'Applications',
    dashboard: 'Dashboard',
    demo: 'Demo',
    settings: 'Settings',
    jobs: 'Jobs',
    job_title: 'Job title',
    misc: 'Misc',
    organization: 'Organisation',
    organizations: 'Organisations',
    create_org: 'Create new company',
    edit_org: 'Edit company',
    del_org: 'Delete company',
    statistics: 'Statistcs',
    create_job: 'Create new Jobad',
    edit_job: 'Edit job ad',
    del_job: 'Delete job ad',
  },

  application_from: {
    title: 'Application Form',
    subtitle: 'Your questions for the applicants',
    ask_candidates_for_qualification:
      'Ask the candidates about their qualifications',
    add_screening_questions:
      'Screeningfragen hinzufügen, um die besten Kandidaten leichter zu finden',
  },
  msg: {
    job_saved_success: 'job posting successfully saved',
  },
  salary: {
    label: 'Salary',
    range: 'Range',
    single_value: 'Single value',
    to: 'to',
    visibility: 'show in job ad',
    hour: 'per hour',
    day: 'per day',
    week: 'per week',
    month: 'per month',
    year: 'per year',
    section_hint:
      'Platforms that offer filtering options to candidates use this information to better target your job.',
    section_desc:
      'This information can highly increase your ranking on some job boards.',
  },
  job: {
    page_title: 'Job Ad Details',
    page_subtitle: 'Edit your Job Ad',
    title_label: 'Job title',
    title_placeholder: 'Enter a job title like e.g. Web Designer',
    intro_label: 'Intro',
    intro_placeholder:
      'Include a brief introduction about your company and the role',
    tasks_label: 'Tasks',
    tasks_placeholder:
      'Enter the job description, include main responsibilities and typical tasks',
    requirements_label: 'Requirements',
    requirements_placeholder:
      'Enter the job requirements, including experience, qualifications, and skills necessary to fulfill the role.',
    benefits_label: 'Benefits',
    benefits_placeholder:
      'List and other perks and benefits that make your company a unique and attractive place to work.',
    outro_label: 'Closing',
    outro_placeholder:
      'Engage candidates with an invitation to apply and a brief description of what to expect regarding the hiring process.',
    submit: 'Proceed to application form',
  },
  location: {
    country: 'Country',
    city: 'City',
    city_placeholder: 'Search city',
    homeoffice_label: 'Remote Job',
    homeoffice_description: 'Job can be performed remotely',
    section_hint:
      'Even if this job is remote, specify a location to ensure it will appear on job boards',
    section_desc:
      'Some platforms list positions by location, so defining one is essential to ensure your job will be visible on all platforms.',
  },

  btn,
  menu,
  label,
  rules,
  help,
};
