/*
 Shared tranlations key should be at root level, and other translations
 should be categorized according to the component/page where they are used.
*/

export default {
  abort: 'Abort',
  apply_postmail: 'Apply by regular post',
  apply_text: 'apply now',
  back: 'Back',
  cancel: 'Cancel',
  close: 'Close',
  continue: 'Next',
  download: 'Download',
  email: 'Email',
  login: 'Login',
  logout: 'Logout',
  preview: 'Preview',
  publish: 'Publish',
  register: 'Register',
  save: 'Save',
  send: 'Send',
};
