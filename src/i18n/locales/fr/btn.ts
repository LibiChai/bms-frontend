/*
 Shared tranlations key should be at root level, and other translations
 should be categorized according to the component/page where they are used.
*/

export default {
  abort: 'Annuler',
  apply_postmail: 'Candidature par courrier',
  apply_text: 'Postulez maintenant',
  back: 'Retour',
  cancel: 'Annuler',
  close: 'Fermez',
  continue: 'Continuer',
  download: 'Téléchargement',
  email: 'Email',
  login: 'Connexion',
  logout: 'Déconnexion',
  preview: 'Aperçu',
  publish: 'Publier',
  register: 'S\'inscrire',
  save: 'Sauver',
  send: 'Envoyer',
};
