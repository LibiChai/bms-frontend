/*
 Shared tranlations key should be at root level, and other translations
 should be categorized according to the component/page where they are used.
*/

export default {
  required: 'Champ obligatoire',
  invalid_date: 'Date non valide',
  invalid_email: {
    delimiters:
      'L\'adresse e-mail semble être incorrecte (vérifier @ et points)',
    unicode_user:
      'Ce nom d\'utilisateur dans l\'e-mail contient des caractères non valides',
    unicode_domain: 'Ce nom de domaine contient des caractères non valides',
    username: 'Le nom d\'utilisateur dans l\'e-mail ne semble pas être valide',
    ip: 'L\'adresse IP de destination n\'est pas valide !',
    domain: 'Le nom de domaine ne semble pas être valide',
    hostname: 'Il manque un nom d\'hôte à cette adresse e-mail !',
  },
  invalid_url: 'URL non valide',
};
