import btn from './btn';
import label from './label';
import rules from './rules';
import help from './help';
import menu from './menu';

export default {
  localeName: 'French',
  english: 'Anglais',
  french: 'Français',
  german: 'Allemand',
  jobs: "Offres d'emploi",
  jobboard: "Bourse d'emploi",
  job_title: "Titre de l'annonce",
  public_jobs: "annonces d'emploi actuelles",
  ad_management: "Gestion d'annonces",
  team_management: "Gestion de l'équipe",
  invite_new_user: "Inviter des membres de l'équipe",
  settings: 'Réglages',
  statistics: 'Statistiques',
  templates: 'Modèles',
  date: 'Date',
  company: 'Entreprise',
  recruiter: 'Recruteur',
  title: 'Veuillez vous identifier.',
  unpublished: 'non publié',
  action: 'Action',
  please_register:
    "Vous n'êtes pas connecté pour le moment. Pour pouvoir utiliser la gestion des annonces, vous devez vous inscrire. L'inscription est gratuite.",
  preis1: "L'inscription est gratuite",
  confirm_del: "Voulez-vous vraiment supprimer l'offre d'emploi ?",
  alert: 'Alerte',
  wizard_help_title: "Créer une offre d'emploi",
  wizard_help_text:
    "Le Jobwizard vous aide à créer une annonce. L'utilisation du Jobwizard et la création de l'annonce sont gratuites. Vous pouvez activer ou désactiver d'autres champs dans les paramètres.",
  wizard_help_anonymous:
    "Vous n'êtes actuellement pas connecté. Vous pouvez utiliser toutes les fonctions en tant qu'utilisateur anonyme. Cependant, vous ne pourrez finalement télécharger l'annonce qu'au format HTML.",
  select_or_create: 'Choisissez une entreprise ou créez-en une nouvelle.',
  welcome: 'Willkommen',
  user_profile: "Profil d'utilisateur",
  company_profile: "Profil de l'entreprise",
  darkmode: {
    label: 'Présentation',
    light: 'Design clair',
    dark: 'Design foncé',
    auto: "État de l'appareil",
  },

  nav: {
    applications: 'Candidatures',
    dashboard: 'Dashboard',
    demo: 'Demo',
    settings: 'Réglages',
    jobs: "Offres d'emploi",
    job_title: "Titre de l'annonce",
    misc: 'Autres',
    organization: 'Organisation',
    organizations: 'Organisations',
    create_org: 'Créer une nouvelle entreprise',
    edit_org: "Modifier l'entreprise",
    del_org: 'Supprimer une entreprise',
    create_job: 'Saisir une nouvelle annonce',
    edit_job: "Modifier l'annonce",
    del_job: "Supprimer une offre d'emploi",
  },

  application_from: {
    title: 'Formulaire de candidaturer',
    subtitle: 'Vos questions aux candidats',
    ask_candidates_for_qualification:
      'Demandez aux candidats quelles sont leurs qualifications',
    add_screening_questions:
      'Ajouter des questions de screening pour trouver plus facilement les meilleurs candidats',
  },
  msg: {
    job_saved_success: "l'offre d'emploi a été sauvegardée avec succès",
  },
  salary: {
    label: 'Salaire',
    range: 'Fourchette de salaire',
    single_value: 'Valeur simple',
    to: "jusqu'à",
    visibility: "Afficher sur l'annonce",
    enter_salary: 'Entrez le salaire',
    hour: 'par heure',
    day: 'par jour',
    week: 'par semaine',
    month: 'par mois',
    year: 'par an',
    section_hint:
      "Des plateformes qui permettent aux candidats une recherche avancée utilisent ce critère afin de mieux positionner leur offre d'emploi",
    section_desc:
      'Cette information a tendance à augmenter votre succès sur certaines plateformes',
  },
  job: {
    page_title: "Details sur l'annonce",
    page_subtitle: "Modifier votre offre d'emploi",
    title_label: 'Titre du poste',
    title_placeholder: 'Saisissez votre titre du poste. Ex. Web Designer',
    intro_label: 'Introduction',
    intro_placeholder:
      "Décrivez brièvement votre entreprise et vos qualités en tant qu'employeur",
    tasks_label: 'Missions principales',
    tasks_placeholder:
      'Précisez les tâches à accomplir, les responsabilités du poste et les missions principales.',
    requirements_label: 'Votre profil',
    requirements_placeholder:
      "Précisez ce que vous attendez des candidats (p.ex. de l'expérience ou des compétences précises souhaitées)",
    benefits_label: 'Avantages',
    benefits_placeholder:
      "Donnez d'autres avantages et bénéfices qui rendent votre entreprise un lieu de travail unique et attrayant.",
    outro_label: 'Informations supplémentaires',
    outro_placeholder:
      "Interagissez avec les candidats avec une invitation à postuler et une description de ce à quoi ils peuvent s'attendre concernant le processus de recrutement. ",
    deadline: 'Date limite de dépôt',
    submit: 'Continuer à "Formulaire de candidature"',
  },
  location: {
    country: 'Pays',
    city: 'Ville',
    city_placeholder: 'Cherchez une ville',
    homeoffice: 'Télétravail',
    homeoffice_description: 'Cet emploi peut être effectué en télétravail',
    section_hint:
      "Afin d'assurer la visibilité de votre offre vous devez indiquer le lieu, même si ce sera en télétravail.",
    section_desc:
      "Certaines platesformes classent les postes par lieu. Il est donc essentiel d'en définir un pour vous assurer que votre poste sera visible sur toutes les platesformes.",
  },
  btn,
  menu,
  label,
  rules,
  help,
};
