/*
 Shared tranlations key should be at root level, and other translations
 should be categorized according to the component/page where they are used.
*/

export default {
  forgot_password:
    "Nous vous envoyons un e-mail avec un lien pour réinitialiser le mot de passe de l'adresse e-mail saisie.",
  dark_mode: 'Activer et désactiver le mode sombre',
  visibility: "Masquer le salaire dans l'offre d'emploi",
  reference:
    "Vous pouvez attribuer un numéro de référence à votre annonce. Un candidat peut s'y référer.",
  apply_post:
    "vous pouvez indiquer ici si vous préférez recevoir les candidatures sous forme traditionnelle par courrier. Le bouton de candidature dans l'offre d'emploi s'adapte alors en conséquence.",
};
