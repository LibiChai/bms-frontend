/*
 Shared tranlations key should be at root level, and other translations
 should be categorized according to the component/page where they are used.
*/

export default {
  forgot_password:
    'Wir senden eine Mail mit einem Link zum Zurücksetzen den Kennworts für die eingegebene E-Mail Adresse.',
  dark_mode: 'Nachtdarstellung ein und ausschalten',
  visibility: 'Gehalt in Stellenanzeige ausblenden',
  reference:
    'Sie können Ihrer Anzeige eine Referenznummer zuweisen. Auf diese kann sich ein Bewerber beziehen.',
  apply_post:
    'hier können sie festlegen, ob sie Bewerbungen lieber in der traditionellen Form per Post wüschen. Der Berwerbungsbutton in der Stellenanzeige passt sich dann entsprechend an.',
};
