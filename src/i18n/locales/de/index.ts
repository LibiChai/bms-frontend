// import text from './text';
// import table from './table';
// import input from './input';
import btn from './btn';
import rules from './rules';
import label from './label';
import menu from './menu';
import help from './help';

export default {
  localeName: 'Deutsch',
  french: 'Französisch',
  german: 'Deutsch',
  english: 'Englisch',
  jobs: 'Stellenanzeigen',
  logo: 'Logo',
  header: 'Headerbild',
  create_job: 'Neue Stellenanzeige eingeben',
  edit_job: 'Stellenanzeige bearbeiten',
  jobboard: 'Stellenbörse',
  public_jobs: 'aktuelle Stellenanzeigen',
  ad_management: 'Anzeigenverwaltung',
  team_management: 'Teamverwaltung',
  invite_new_user: 'Teammitglieder einladen',
  job_title: 'Anzeigentitel',
  settings: 'Einstellungen',
  templates: 'Vorlagen',
  company: 'Firma',
  recruiter: 'Rekruter',
  unpublished: 'unveröffentlicht',
  title: 'Bitte melden Sie sich an',
  action: 'Aktion',
  'ad-management-description':
    'Mit der Anzeigenverwaltung können die Stellenanzeigen speichern, bearbeiten und löschen. Die Anzeigenverwaltung wird auch benötigt, um Stellenanzeigen in ihre Homepage zu integrieren.',
  preis1: 'Die Anmeldung ist kostenlos',
  please_register:
    'Sie sind momentan nicht angemeldet. Um die Anzeigenverwaltung zu nutzen, müssen Sie sich registrieren. Die Registrierung ist kostenlos.',
  alert: 'Achtung',
  wizard_help_title: 'Stellenanzeige erstellen',
  wizard_help_text:
    'Der Jobwizard unterstützt sie bei der Erstellung einer Anzeige. Die Nutzung des Jobwizard und die Erstellung der Anzeige ist kostenlos. Über die Einstellungen können sie weitere Felder aktivieren und deaktivieren.',
  wizard_help_anonymous:
    'Sie sind momentan nicht angemeldet. Sie können als anonymer Benutzer alle Funktionen nutzen. Allerdings können Sie am Ende die Anzeige nur als HTML downloaden.',
  select_or_create: 'Wählen sie eine Firma oder erstellen sie eine neue.',
  welcome: 'Willkommen',
  user_profile: 'Benutzerprofil',
  company_profile: 'Unternehmensprofil',

  darkmode: {
    label: 'Darstellung',
    light: 'Helles Design',
    dark: 'Dunkles Design',
    auto: 'Gerätezustand',
  },
  nav: {
    applications: 'Bewerbungen',
    dashboard: 'Dashboard',
    demo: 'Demo',
    settings: 'Einstellungen',
    jobs: 'Stellenanzeigen',
    job_title: 'Job title',
    organizations: 'Firmen',
    misc: 'Sonstiges',
    statistics: 'Statistiken',
    organization: 'Firma',
    create_org: 'Neue Firma anlegen',
    edit_org: 'Firma bearbeiten',
    del_org: 'Firma löschen',
    edit_job: 'Anzeige bearbeiten',
    del_job: 'Anzeige löschen',
  },
  application_form: {
    title: 'Bewerbungsformular',
    subtitle: 'Ihre Fragen an die Bewerber',
    ask_candidates_for_qualification:
      'Fragen Sie die Kandidaten nach ihren Qualifikationen',
    add_screening_questions:
      'Screeningfragen hinzufügen, um die besten Kandidaten leichter zu finden',
    language: 'Sprache des Bewerbungsformulars',
  },
  dialog: {
    confirm_delete_user: 'Möchten sie wirklich den Benutzer "{u}" löschen?',
  },
  employment_type: {
    label: 'Anstellungsart',
  },
  mail: {
    application_received: 'Bewerbungseingang',
    application_received_caption:
      'Wenn ein Kandidat sich auf eine Ihrer Stellen bewirbt, senden wir diesem umgehend die unten stehende Nachricht.',
    reject_after_first_review: 'Ablehnung nach Prüfung der Unterlagen',
    reject_after_first_review_caption:
      'Diese Nachricht wird gesendet, nachdem Sie die Bewerbungsunterlagen eines Bewerbers geprüft und ihn als „nicht qualifiziert“ eingestuft haben.',
    reject_during_process: 'Ablehnung im späteren Verlauf',
    reject_during_process_caption:
      'Diese Nachricht wird am nächsten Morgen verschickt, wenn ein Bewerber zuerst als „qualifiziert“ markiert wurde und dann als „nicht qualifiziert“ eingestuft wird, nachdem er einen Teil Ihres Prozesses durchlaufen hat (z. B. nach einem zweiten Screening / einem Interview / einer Arbeitsprobe).',
    placeholders_help:
      'Um dynamische Informationen einzufügen, können Sie [firstName], [lastName], [companyName] oder [jobTitle] verwenden.',
    language: 'Sprache der Nachrichten',
    language_caption:
      'Automatische Nachrichten werden in der gleichen Sprache wie das Bewerbungsformular gesendet. Sie können die Sprache auf dem Bildschirm "Details zur Stellenanzeige" ändern.',
    help: 'Was sind automatisierte Nachrichten?',
    help_caption:
      'Automatisierte Nachrichten werden versendet, sobald Bewerber eine vordefinierte Stufe durchlaufen. Die hier gezeigten E-Mail-Vorlagen werden dabei als Standard verwendet - der Inhalt der Nachrichten kann jedoch pro Job angepasst werden. Automatisierte Nachrichten können sowohl standardmäßig für alle Jobs als auch individuell pro Job aktiviert oder deaktiviert werden.',
  },
  role: {
    company_owner: 'Hauptbenutzer',
    recruiter: 'Rekruter',
    authenticated: 'Authentifiziert',
  },
  confirm: {
    delete_organization: 'Möchten Sie die Firma wirklich gelöscht werden?',
    delete_job: 'Möchten Sie die Stellenanzeige wirklich löschen?',
  },
  salary: {
    label: 'Gehalt',
    range: 'Gehaltsspanne',
    single_value: 'Einzelwert',
    to: 'bis',
    visibility: 'in der Stellenanzeige anzeigen',
    enter_salary: 'Gehalt eingeben',
    hour: 'pro Stunde',
    day: 'pro Tag',
    week: 'pro Woche',
    month: 'pro Monat',
    year: 'pro Jahr',
    section_hint:
      'Plattformen, die den Kandidaten Filteroptionen anbieten, nutzen diese Informationen, um Ihre Stelle besser zu positionieren.',
    section_desc:
      'Diese Information kann Ihre Position in einigen Jobbörsen erheblich verbessern.',
  },

  job: {
    page_title: 'Angaben zur Stellenanzeige',
    page_subtitle: 'Stellenanzeige anpassen',
    title_label: 'Stellentitel',
    title_placeholder: 'z.B. Web Designer',
    intro_label: 'Einleitung',
    intro_placeholder:
      'Beschreiben Sie kurz und knapp Ihr Unternehmen und was Sie zu einem attraktiven Arbeitgeber macht.',
    tasks_label: 'Aufgaben',
    tasks_placeholder:
      'Beschreiben Sie die genauen Aufgaben und Veranwortungsbereiche der Stelle, damit potenzielle Bewerber verstehen, was diese Position attraktiv macht.',
    requirements_label: 'Qualifikation',
    requirements_placeholder:
      'Beschreiben Sie Ihre Anforderungen an den Kandidaten wie z.B. gewünschte Berufserfahrung, Qualifikationen und Fähigkeiten.',
    benefits_label: 'Benefits',
    benefits_placeholder:
      'Nennen Sie weitere Benefits, die Ihr Unternehmen zu einem einzigartigen und attraktiven Arbeitsplatz machen.',
    outro_label: 'Noch ein paar Worte zum Schluss',
    outro_placeholder: 'wie z.B. Wir freuen uns über deine Bewerbung!',
    deadline: 'Bewerbungsfrist',
    submit: 'Weiter zum Bewerbungsformular',
  },
  location: {
    country: 'Land',
    city: 'Stadt',
    city_placeholder: 'Stadt suchen',
    homeoffice: 'Remote Job',
    homeoffice_description: 'Job kann im Home Office ausgeführt werden',
    section_hint:
      'Selbst wenn diese Stelle Remote ausgeschrieben ist, geben Sie einen Ort an, um sicherzustellen, dass sie in den Jobbörsen erscheint',
    section_desc:
      'Die Plattformen listen die Positionen nach dem Standort auf, daher ist es wichtig, einen Standort zu definieren, um sicherzustellen, dass Ihre Anzeige für die Kandidaten sichtbar ist.',
  },
  yes: 'Ja',
  no: 'Nein',
  published: 'veröffentlicht',
  draft: 'in Bearbeitung',

  btn,
  help,
  menu,
  label,
  rules,
};
