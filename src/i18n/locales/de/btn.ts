/*
 Shared tranlations key should be at root level, and other translations
 should be categorized according to the component/page where they are used.
*/

export default {
  abort: 'Abbrechen',
  apply_postmail: 'Bewerbung per Post',
  apply_text: 'jetzt bewerben',
  back: 'Zurück',
  cancel: 'Abbrechen',
  close: 'Schliessen',
  continue: 'Weiter',
  download: 'Download',
  email: 'E-Mail',
  login: 'Anmelden',
  logout: 'Abmelden',
  preview: 'Vorschau',
  publish: 'Veröffentlichen',
  register: 'Registrieren',
  save: 'Speichern',
  send: 'Senden',
};
