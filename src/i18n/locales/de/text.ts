/*
 Shared tranlations key should be at root level, and other translations
 should be categorized according to the component/page where they are used.
*/

export default {
  localeName: 'Deutsch',
  french: 'Französisch',
  german: 'Deutsch',
  english: 'Englisch',
  jobs: 'Stellenanzeigen',
  logo: 'Logo',
  header: 'Headerbild',

  company: {
    title: 'Karriereseite',
    caption: 'Sichtbarkeit ihres Unternehmens',
    tab: {
      company: 'Firma',
      gallery: 'galery',
    },
  },
};
