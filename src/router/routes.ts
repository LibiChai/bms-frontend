import type { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    component: async () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: async () => import('pages/IndexPage.vue') },
      {
        path: '/profile',
        component: async () => import('pages/personnel/UserProfile.vue'),
      },
      {
        path: '/team',
        name: 'team',
        component: async () => import('pages/TeamManager.vue')
      },
      { path: '/news', component: async () => import('pages/NotificationsPage.vue') },
      { path: '/api', component: async () => import('pages/ApiPage.vue') },
      {
        path: '/company',
        component: async () => import('pages/CompanyProfile.vue'),
      },
      { path: '/jobs', component: async () => import('pages/JobsList.vue') },
      {
        path: '/job/:id',
        component: async () => import('pages/JobManager.vue'),
      },
      {
        path: '/invoice',
        component: async () => import('pages/InvoicePage.vue'),
      },
      {
        path: '/widget',
        component: async () => import('pages/WidgetPage.vue'),
      },
      {
        path: '/account',
        component: async () => import('pages/AccountPage.vue'),
      },
      {
        path: '/notifications',
        component: async () => import('pages/NotificationsPage.vue'),
      },
      {
        path: '/integrations',
        name: 'integration',
        component: async () => import('pages/IntegrationPage.vue'),
      },
      {
        path: '/application-form',
        name: 'application-form',
        component: async () => import('pages/ApplicationForm.vue'),
      },
      {
        path: '/messages',
        name: 'messages',
        component: async () => import('pages/MessagesPage.vue'),
      },
      {
        path: '/candidates',
        component: async () => import('src/pages/CandidatesList.vue'),
      },
      {
        path: '/candidate/:id',
        name: 'candidate',
        component: async () => import('src/pages/CandidateDetail.vue'),
      },
      {
        path: '/login',
        name: 'Login',
        component: async () => import('src/pages/StrapiLogin.vue'),
      },
      {
        path: '/public/job/:id',
        component: async () => import('src/pages/public/JobPosting.vue'),
      },
      {
        path: '/public/forms',
        component: async () => import('src/pages/public/ApplicationForm.vue'),
      },
      {
        path: '/public/company',
        component: async () => import('src/pages/public/CompanyProfile.vue'),
      }
    ],


  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: async () => import('pages/ErrorNotFound.vue'),
  },
];

export default routes;
