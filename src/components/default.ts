'use strict';

export const defaults = {
  pagination: {
    page: 1,
    rowsPerPage: 25,
    sortBy: 'updatedAt',
    descending: false,
  },
  rowsPerPage: [10, 25, 50],
  filterJobs: {
    q: '',
  },
  filterCandidates: {
    q: '',
    job: ''
  },
  icons: {
    fileGeneral: 'description',
    fileDocx: 'img:/icons/mime-docx.svg',
    fileOpenoffice: 'img:/icons/mime-openoffice.svg',
    filePdf: 'img:/icons/mime-pdf.svg',
    fileTxt: 'img:/icons/mime-txt.svg'
  }
};
