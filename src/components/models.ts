export type Array = {
  id: number;
  value: string;
};

export type Photo = {
  id?: number;
  formats: Array;
  url: Text;
  attributes?: Array
}

export type Form = {
  id?: number;
  name:Text;
  locale: Text;
  questions: Array;
}

export type Message = {
  id?: number;
  subject?: Text;
  message?: Text;
  attributes?: Array
}

export type AttachmentsAttributes = {
  test: Text;
}

export type Attachment = {
  id?: number;
  formats: Array;
  url: Text;
  attributes?: AttachmentsAttributes
}

export type User = {
  id?: number;
  firstname: Text;
  lastname: Text;
  email: Text;
  interface_mode: Text;
  preferred_language: Text;
  darkmode: boolean | 'auto';
};

export type Company = {
  id?: number,
  name: Text
}

export type Employee = {
  id?: number,
  user: User,
  Company: Company
}

export type Job = {
  id?: number;
  title: Text;
  intro: Text;
  tasks: Text;
  requirements: Text;
  benefits: Text;
  outro: Text;
  salary: {
    min: number;
    max: number;
    currency: Text;
    intervall: Text;
    visible: true;
  };
  education: Array;
  experience: Array;
  contract: Array;
  career_level: Array;
  category: Text;
  application_lang: Array;
  employment_type: Array;
  deadline: Text;
  country: Text;
  city: Text;
  remote: Text;
};

export type Candidate = {
  id?: number;
  firstname: Text;
  lastname: Text;
  phone: Text;
  email: Text;
  postalcode: Text;
  city: Text;
  street: Text;
};

export type StrapiParams = {
  _q?: Text,
  filters?: Array
  populate?: Text,
  sort: Text
}
