import {boot} from 'quasar/wrappers';
import type {AxiosInstance} from 'axios';
import axios from 'axios';

import {useAuthStore} from 'stores/auth';
import {Notify} from 'quasar'

declare module '@vue/runtime-core' {
  type ComponentCustomProperties = {
    $axios: AxiosInstance;
  };
}

// Be careful when using SSR for cross-request state pollution
// due to creating a Singleton instance here;
// If any client changes this (global) instance, it might be a
// good idea to move this instance creation inside of the
// "export default () => {}" function below (which runs individually
// for each client)
const api = axios.create({baseURL: import.meta.env.VITE_AUTH_URL});
api.defaults.headers.common.Accept = 'application/json';

api.interceptors.request.use(function (config) {
  const store = useAuthStore();
  const token = store.token;
  if (token && store.isLoggedIn) {
    config.headers.Authorization = `Bearer ${token}`
  }
  return config;
});
api.interceptors.response.use(function (response) {
  // Do something with response data
  return response;
}, function (error) {
  const store = useAuthStore();
  const token = store.token;
  if (error && !error.response && !error.status) {
    Notify.create({
      type: 'negative',
      message: 'Backend not available',
      position: 'bottom'
    })
  } else if (error.response.status == 401 && token) {
    store.refresh();
  } else if (error.response.status == 403) {
    const err = error.response.data?.error;
    Notify.create({
      type: 'negative',
      message: err?.message,
      position: 'bottom'
    })
  }
});
// if (auth.isLoggedIn) {
//  api.defaults.headers.common.Authorization = 'Bearer ' + auth.token;
// }

export default boot(({app}) => {
  // For use inside Vue files (Options API) through this.$axios and this.$api

  app.config.globalProperties.$axios = axios;
  // ^ ^ ^ this will allow you to use this.$axios (for Vue Options API form)
  //       so you won't necessarily have to import axios in each vue file

  app.config.globalProperties.$api = api;
  // ^ ^ ^ this will allow you to use this.$api (for Vue Options API form)
  //       so you can easily perform requests against your app's API
});

export {api};
