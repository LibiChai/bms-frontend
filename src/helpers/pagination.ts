import { Pagination } from 'src/types/pagination';

const preparePagination = (data: Pagination) => {
  return {
    page: data.paginator.page,
    rowsPerPage: data.paginator.pageSize,
    sortBy: data.sort.split(':')[0],
    descending: data.sort.split(':')[1] === 'desc',
    rowsNumber: data.paginator.total,
  };
};

export { preparePagination };
