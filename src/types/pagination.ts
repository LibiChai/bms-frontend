export interface Paginator {
  page: number | undefined;
  pageSize: number | undefined;
  total: number | undefined;
}
export interface Pagination {
  paginator: Paginator;
  sort: string;
}
