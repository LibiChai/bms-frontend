'use strict';

import {Candidate, StrapiParams} from 'components/models';
import {api} from 'boot/axios';
import {defineStore} from 'pinia';
import {preparePagination} from 'src/helpers/pagination';

api.defaults.headers.common.Accept = 'application/json';

export const useCandidateStore = defineStore('candidate', {
  state: () => ({
    isLoading: false,
    candidate: {
      id: null,
      firstname: '',
      lastname: '',
    },
    candidates: {
      result: [],
      filter: '',
      sort: 'id:asc',
      paginator: {
        page: undefined,
        pageSize: undefined,
        pageCount: undefined,
        total: undefined,
      },
    },
  }),

  getters: {
    getCandidate(state) {
      return state.candidate;
    },
    getCandidates(state) {
      return state.candidates;
    },
    getPagination(state) {
      return preparePagination(state.candidates);
    },
  },

  actions: {
    async save(candidate: Candidate) {
      this.isLoading = true
      if (candidate && candidate.id) {
        const id = candidate.id;
        delete candidate.id;

        api
          .put(`/api/candidates/${id}`, {data: candidate})
          .then((r) => {
            this.candidate = r.data;
          })
          .finally(() => {
            this.isLoading = false
          });
      } else {
        api
          .post('/api/candidates', {data: candidate})
          .then((r) => {
            this.candidate = r.data;
          })
          .finally(() => {
            this.isLoading = false
          });
      }
    },
    async fetchCandidates(props) {
      this.isLoading = true;

      const {sortBy, descending, page, rowsPerPage} = props.pagination;

      const params: StrapiParams = {
        'pagination[page]': page ?? 1,
        'pagination[pageSize]': rowsPerPage ?? 10,
        populate: '*',
      };

      if (sortBy) {
        params.sort = `${sortBy}:${descending ? 'desc' : 'desc'}`;
      }

      if (props.filter && props.filter.q && typeof props.filter.q.valueOf() === 'string') {
        params._q = props.filter.q;
      }

      if (props.filter && props.filter.job) {
        params['filters[job]'] = props.filter.job.value
      }

      api
        .get('/api/candidates', {
          params
        })
        .then((response) => {
          this.candidates.result = response.data.data;
          this.candidates.paginator = {...response.data.meta.pagination};
        })
        .finally(() => {
          this.isLoading = false;
        });
    },
    async fetchCandidate(id) {
      this.isLoading = true;

      api
        .get(`/api/candidates/${id}`, {
          params: {
            populate: '*',
          },
        })
        .then((response) => {
          this.candidate = {
            id: response.data.data.id,
            ...response.data.data.attributes
          };
        })
        .finally(() => {
          this.isLoading = false;
        });
    },
  },
  persist: false,
});
