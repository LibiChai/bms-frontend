'use strict';

import { Employee } from 'components/models';
import { api } from 'boot/axios';
import { defineStore } from 'pinia';
import { preparePagination } from 'src/helpers/pagination';

api.defaults.headers.common.Accept = 'application/json';

export const useEmployeeStore = defineStore('employee', {
  state: () => ({
    isLoading: false,
    employee: {
      id: null,
      user: {},
      company: {}
    },
    employees: {
      result: [],
      filter: '',
      sort: 'id:asc',
      paginator: {
        page: undefined,
        pageSize: undefined,
        pageCount: undefined,
        total: undefined,
      },
    },
  }),

  getters: {
    getJob(state) {
      return state.employee;
    },
    getJobs(state) {
      return state.employees;
    },
    getPagination(state) {
      return preparePagination(state.employees);
    },
  },

  actions: {
    async count() {
      console.log('Log');
    },
    async save(employee: Employee) {
      this.isLoading = true
      if (employee && employee?.id) {
        const id = employee.id;
        delete employee.id;

        api
          .put(`/api/employees/${id}`, { data: employee })
          .then((r) => {
            this.employee = r.data;
          })
          .finally(()=>{this.isLoading=false});;
      } else {
        const data = { ...employee }
        api
          .post('/api/employees', { data })
          .then((r) => {
            console.log('CREATES', r);
            this.employee = r.data;
          })
          .finally(()=>{this.isLoading=false});
      }
    },
    async delete(id) {
      this.isLoading = true;

      api
        .delete('/api/employees/' + id).then((r) => {
          console.log('DELETE', r)
        })
        .finally(() => {
          this.isLoading = false;
        });

    },
    async fetchEmployees(props: { filter: string, pagination }) {
      this.isLoading = true;
      const { sortBy, descending, page, rowsPerPage } = props.pagination;
      const params = {
        'pagination[page]': page ?? 1,
        'pagination[pageSize]': rowsPerPage ?? 10,
        populate: '*',
      };

      if (sortBy) {
        params.sort = `${sortBy}:${descending ? 'desc' : 'desc'}`;
      }

      if (props.filter && typeof props.filter.valueOf() === 'string') {
        params._q = props.filter;
      }

      api
        .get('/api/employees', {
          params,
        })
        .then((response) => {
          this.employees.result = response.data.data;
          this.employees.paginator = { ...response.data.meta.pagination };
        })
        .catch((e) => {
          console.log(e);
        })
        .finally(() => {
          this.isLoading = false;
        });
    }
  },
  persist: false,
});
