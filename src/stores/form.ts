'use strict';

import { Form } from 'components/models';
import { api } from 'boot/axios';
import { defineStore } from 'pinia';
import { preparePagination } from 'src/helpers/pagination';

api.defaults.headers.common.Accept = 'application/json';

export const useFormStore = defineStore('form', {
  state: () => ({
    isLoading: false,
    form: {
      id: null,
      name: '',
    },
    forms: {
      result: [],
      filter: '',
      sort: 'id:asc',
      paginator: {
        page: undefined,
        pageSize: undefined,
        pageCount: undefined,
        total: undefined,
      },
    },
  }),

  getters: {
    getForm(state) {
      return state.form;
    },
    getForms(state) {
      return state.forms;
    },
    getPagination(state) {
      return preparePagination(state.forms);
    },
  },

  actions: {
    async save(data: Form) {
      console.log('Start save action');
      if (data.id) {
        const id = data.id;
        delete data.id;

        api
          .put(`/api/application-forms/${id}`, { data: data })
          .then((r) => {
            this.form = r.data;
          })
          .catch((e) => {
            console.log(e);
          });
      } else {
        api
          .post('/api/application-forms', { data: data })
          .then((r) => {
            console.log('CREATES', r);
            this.form = r.data;
          })
          .catch((e) => {
            console.log(e);
          });
      }
    },
    async fetchForms(props: { filter: string, pagination }) {
      this.isLoading = true;
      const { sortBy, descending, page, rowsPerPage } = props.pagination;
      const params = {
        'pagination[page]': page ?? 1,
        'pagination[pageSize]': rowsPerPage ?? 10,
        populate: '*',
      };

      if (sortBy) {
        params.sort = `${sortBy}:${descending ? 'desc' : 'desc'}`;
      }

      if (props.filter && typeof props.filter.valueOf() === 'string') {
        params._q = props.filter;
      }

      try {
        api
          .get('api/application-forms', {
            params
          })
          .then((response) => {
            this.forms.result = response.data.data;
            this.forms.paginator = { ...response.data.meta.pagination };
          })
          .catch((e) => {
            console.log(e);
          }).finally(()=> {
            this.isLoading = false
          });
      } catch (e) {
        console.log(e);
      }
    },
    async fetchForm(id) {
      const params = {
        populate: '*',
      };

      try {
        api
          .get(`api/application-form/${id}`, {
            params,
            headers: config.headers,
          })
          .then((response) => {
            this.form = response.data.data;
          })
          .catch((e) => {
            console.log(e);
          });
      } catch (e) {
        console.log(e);
      }
    },
  },
  persist: false,
});
