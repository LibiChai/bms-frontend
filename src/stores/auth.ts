'use strict';

import {api} from 'boot/axios';
import {defineStore} from 'pinia';
import {preparePagination} from 'src/helpers/pagination';

export const useAuthStore = defineStore('auth', {
  state: () => ({
    token: null,
    user: {
      id: null,
      firstname: '',
      lastname: '',
      username: '',
      email: '',
      preferedLanguage: '',
      darkmode: 'auto',
      createdAt: null,
      updatedAt: null,
      company: null,
    },
    users: {
      result: [],
      filter: '',
      sort: 'id:asc',
      paginator: {
        page: undefined,
        pageSize: undefined,
        pageCount: undefined,
        total: undefined,
      },
    },
    isLoggedIn: false,
  }),

  getters: {
    getUser(state) {
      return state.user;
    },
    getJobs(state) {
      return state.users;
    },
    getPagination(state) {
      return preparePagination(state.users);
    },
  },

  actions: {
    async loginUser(username, password) {
      const data = {
        identifier: username,
        password: password,
        populate: 'company',
      };
      try {
        const result = await api.post('/api/auth/local', data);
        this.user = result.data.user;
        this.token = result.data.jwt;
        this.isLoggedIn = true;
      } catch (error) {
        if (error.response) {
          throw new Error(error.response.data.error.message);
        }
      }
    },
    async registerUser(data) {
      api
        .post('/api/auth/local/register', {
          username: data.username,
          email: data.email,
          password: data.password,
        })
        .then((response) => {
          this.token = response.data.jwt;
          this.user = response.data.user;
        })
        .catch((error) => {
          if (error.response) {
            throw new Error(error.response.data.error.message);
          }
        });
    },
    async forgotPassword(data) {
      api
        .post('/api/auth/forgot-password', {
          email: data.email,
        })
        .then((response) => {
          console.log(response);
        })
        .catch((error) => {
          if (error.response) {
            throw new Error(error.response.data.error.message);
          }
        });
      console.log(data);
    },
    async refresh() {
      const data = {
        refreshToken: this.token,
      };
      const options = {
        'Access-Control-Allow-Credentials': true,
        withCredentials: true,
      };
      api.post('/api/token/refresh', data, options).then((response) => {
        this.token = response?.data?.jwt;
      });
    },
    logout() {
      this.$reset();
    },
  },
  persist: {
    storage: localStorage,
    paths: ['token', 'user', 'isLoggedIn'],
  },
});
