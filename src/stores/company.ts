'use strict';

import { api } from 'boot/axios';
import { defineStore } from 'pinia';

api.defaults.headers.common.Accept = 'application/json';

export const useCompanyStore = defineStore('company', {
  state: () => ({
    isLoading: false,
    company: {
      id: null,
      name: '',
    }
  }),

  getters: {
    getCompany(state) {
      return state.company;
    },
  },

  actions: {
    async fetchCompany(id) {
      const params = {
        populate: '*',
      };

      try {
        api
          .get(`api/companies/${id}`, { params })
          .then((response) => {
            this.company = response.data.data;
          })
      } catch (e) {
        console.log(e);
      }
    },
  },
  persist: false,
});
