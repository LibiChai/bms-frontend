'use strict';

import {Job} from 'components/models';
import {api} from 'boot/axios';
import {defineStore} from 'pinia';
import {preparePagination} from 'src/helpers/pagination';

api.defaults.headers.common.Accept = 'application/json';

export const useJobStore = defineStore('job', {
  state: () => ({
    isLoading: false,
    job: {
      id: null,
      title: '',
      intro: '',
      tasks: '',
      requirements: '',
      benefits: '',
      outro: '',
      salary: {
        min: '',
        max: '',
        currency: 'EUR',
        intervall: 'year',
        visible: true,
      },
      education: [],
      experience: [],
      contract: [],
      career_level: [],
      category: '',
      application_lang: [],
      employment_type: [],
      deadline: '',
      country: '',
      city: '',
      remote: '',
    },
    jobs: {
      result: [],
      filter: '',
      sort: 'id:asc',
      paginator: {
        page: undefined,
        pageSize: undefined,
        pageCount: undefined,
        total: undefined,
      },
    },
  }),

  getters: {
    getJob(state) {
      return state.job;
    },
    getJobs(state) {
      return state.jobs;
    },
    getPagination(state) {
      return preparePagination(state.jobs);
    },
  },

  actions: {
    async count() {
      console.log('Log');
    },
    async save(job: Job) {
      console.log('Start save action');
      if (job.id) {
        const id = job.id;
        delete job.id;

        api
          .put(`/api/jobs/${id}`, {data: job})
          .then((r) => {
            this.job = r.data;
          })
          .catch((e) => {
            console.log(e);
          });
      } else {
        api
          .post('/api/jobs', {data: job})
          .then((r) => {
            console.log('CREATES', r);
            this.job = r.data;
          })
          .catch((e) => {
            console.log(e);
          });
      }
    },
    async fetchJobs(props: { filter: string, pagination }) {
      this.isLoading = true;
      const {sortBy, descending, page, rowsPerPage} = props.pagination;

      const params = {
        'pagination[page]': page ?? 1,
        'pagination[pageSize]': rowsPerPage ?? 10,
        populate: '*',
      };

      if (sortBy) {
        params.sort = `${sortBy}:${descending ? 'desc' : 'desc'}`;
      }

      if (props.filter && typeof props.filter.valueOf() === 'string') {
        params._q = props.filter;
      }

      try {
        api
          .get(`api/jobs${props.status ? '?status=1' : ''}`, {
            params,
          })
          .then((response) => {
            this.jobs.result = response.data.data;
            this.jobs.paginator = {...response.data.meta.pagination};
          })
          .catch((e) => {
            console.log(e);
          }).finally(() => {
          this.isLoading = false
        });
      } catch (e) {
        console.log(e);
      }
    },
    async fetchJob(id) {
      const params = {
        populate: '*',
      };

      try {
        api
          .get(`api/jobs/${id}`, {
            params,
          })
          .then((response) => {
            this.job = response.data.data;
          })
          .catch((e) => {
            console.log(e);
          });
      } catch (e) {
        console.log(e);
      }
    },
  },
  persist: false,
});
