const required = v => (v && v.length > 0) || 'rules.required';

const validEmail = v => v
  ? /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(v)
        || 'rules.invalidEmail'
  : true;

export default {
  required,
  validEmail,
};
